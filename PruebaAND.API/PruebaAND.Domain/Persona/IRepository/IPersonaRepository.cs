﻿using PruebaAND.Domain.Persona.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Domain.Persona.IRepository
{
    public interface IPersonaRepository
    {
        ICollection<PersonaEntity> ListarPersonas();
    }
}
