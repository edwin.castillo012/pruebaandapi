﻿using PruebaAND.Domain.Persona.Entity;
using PruebaAND.Domain.Persona.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Domain.Persona.Service
{
    public class PersonaService
    {
        private readonly IPersonaRepository _personaRepository;

        public PersonaService(IPersonaRepository personaRepository) 
        { 
            _personaRepository = personaRepository;
        }
        public ICollection<PersonaEntity> ListarPersonas()
        {
            try
            {
                return _personaRepository.ListarPersonas();
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
