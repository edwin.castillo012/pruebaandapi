﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using PruebaAND.Application.DTO;
using PruebaAND.Application.Models;
using PruebaAND.Application.Profile;
using PruebaAND.Application.UseCases.Interface;
using PruebaAND.Domain.Persona.Service;
using PruebaAND.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Application.UseCases
{
    public class PersonaUseCase : IPersonaUseCase
    {
        private readonly IMapper _mapper;

        public PersonaUseCase()
        {
            var mapConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ApplicationProfile>();
                cfg.AddExpressionMapping();
            });
            this._mapper = new Mapper(mapConfig);
        }

        public Response<ICollection<PersonaDTO>> ListarPersonas()
        {
            var response = new Response<ICollection<PersonaDTO>>();

            try
            {
                using (var personaRepository = new PersonaRepository())
                {
                    response.Data = _mapper.Map<ICollection<PersonaDTO>>(new PersonaService(personaRepository).ListarPersonas());
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
                response.Succeeded = false;
            }

            return response;
        }
    }
}
