﻿using PruebaAND.Application.Models;
using PruebaAND.Application.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Application.UseCases.Interface
{
    public interface IPersonaUseCase
    {
        Response<ICollection<PersonaDTO>> ListarPersonas();
    }
}
