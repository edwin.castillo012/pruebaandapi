﻿using PruebaAND.Application.DTO;
using PruebaAND.Domain.Persona.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Application.Profile
{

    public class ApplicationProfile : AutoMapper.Profile
    {
        public ApplicationProfile()
        {
            CreateMap<PersonaDTO, PersonaEntity>().ReverseMap();
        }
    }
}
