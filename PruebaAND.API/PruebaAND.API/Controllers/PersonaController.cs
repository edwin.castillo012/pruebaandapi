﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PruebaAND.Application.UseCases;
using PruebaAND.Application.UseCases.Interface;

namespace PruebaAND.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        //Creando Objeto de tipo interfaz para poder utilizar las funciones del caso de uso.
        private readonly IPersonaUseCase _personaUseCase;

        public PersonaController()
        {
            _personaUseCase = new PersonaUseCase();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult ListarPersonas()
        {
            try
            {
                var result = _personaUseCase.ListarPersonas();
                if (!result.Succeeded)
                    return BadRequest();
                return new JsonResult(result);
            }
            catch (Exception e)
            {
                //TODO: log error
                return new JsonResult(e.Message);
            }
        }
    }
}
