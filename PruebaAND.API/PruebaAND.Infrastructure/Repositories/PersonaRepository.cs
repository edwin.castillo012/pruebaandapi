﻿using PruebaAND.Domain.Persona.Entity;
using PruebaAND.Domain.Persona.IRepository;
using PruebaAND.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAND.Infrastructure.Repositories
{
    public class PersonaRepository : IPersonaRepository, IDisposable
    {
        public void Dispose()
        {
        }

        public ICollection<PersonaEntity> ListarPersonas()
        {
            try
            {

                ICollection<PersonaEntity> lista = new Collection<PersonaEntity>();
                PersonaEntity persona0 = new PersonaEntity()
                {
                    Id = 1,
                    Nombre = "Pepita Perez",
                    Correo = "pepita@gmail.com",
                    Edad = 25,
                    Telefono = "3123456789",
                    Direccion = "Calle 4 sur # 50-60",
                    Estado = true,
                };

                PersonaEntity persona1 = new PersonaEntity();
                persona1.Id = 1;
                persona1.Nombre = "Pepito Perez";
                persona1.Correo = "pepito@gmail.com";
                persona1.Edad = 25;
                persona1.Telefono = "3123456789";
                persona1.Direccion = "Calle 4 sur # 50-60";
                persona1.Estado = true;

                PersonaEntity persona2 = new PersonaEntity();
                persona2.Id = 2;
                persona2.Nombre = "Camilo Cifuentes";
                persona2.Correo = "camilo@gmail.com";
                persona2.Edad = 30;
                persona2.Telefono = "3153425678";
                persona2.Direccion = "Calle 2 sur # 17-11";
                persona2.Estado = true;

                PersonaEntity persona3 = new PersonaEntity();
                persona3.Id = 3;
                persona3.Nombre = "Carlos Parra";
                persona3.Correo = "carlos@gmail.com";
                persona3.Edad = 35;
                persona3.Telefono = "3245678976";
                persona3.Direccion = "Calle 3 sur # 20-21";
                persona3.Estado = true;

                PersonaEntity persona4 = new PersonaEntity();
                persona4.Id = 4;
                persona4.Nombre = "Laura Romero";
                persona4.Correo = "laura@gmail.com";
                persona4.Edad = 23;
                persona4.Telefono = "31234567846";
                persona4.Direccion = "Calle 5 sur # 23-45";
                persona4.Estado = true;

                PersonaEntity persona5 = new PersonaEntity();
                persona5.Id = 5;
                persona5.Nombre = "Edwin Castillo";
                persona5.Correo = "edwin@gmail.com";
                persona5.Edad = 25;
                persona5.Telefono = "3213678543";
                persona5.Direccion = "Calle 2A sur # 13-23";
                persona5.Estado = true;

                lista.Add(persona0);
                lista.Add(persona1);
                lista.Add(persona2);
                lista.Add(persona3);
                lista.Add(persona4);
                lista.Add(persona5);

                return lista;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
